import { auth } from '../_constant';
import Keycloak from '../../node_modules/keycloak-js';
const kc = new Keycloak({ url: 'http://localhost:8080/auth', realm: 'demo', clientId: 'bookBox'});

const initialState = {
    keycloak: kc,
    authenticated: false,
    user: false,
    merchant: false
};

export function login(state = initialState, action) {
    switch (action.type) {
        case auth.SET_AUTHENTICATED:
            return Object.assign({}, state, { authenticated: action.payload.authenticated, user: action.payload.user, merchant: action.payload.merchant});
        default:
            return state
    }
}