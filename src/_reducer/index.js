import { combineReducers } from '../../node_modules/redux';
import { login } from './auth.keycloak';

const rootReducer = combineReducers({
    login
});

export default rootReducer;