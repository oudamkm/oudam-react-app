import React from '../node_modules/react';
import { render } from '../node_modules/react-dom';
import { Provider } from '../node_modules/react-redux';

import { store } from './_store';
import { App } from './app/App';

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('app')
);