import React from 'react';
import { connect } from 'react-redux';
import { loginAction } from '../../_action';
import { ROLE } from '../../_constant';

class Protected extends React.Component {

    constructor(props){
        super(props);
    }

    componentWillReceiveProps(nextState){
        if(nextState.auth){
            console.log("Test => " + nextState.auth.authenticated);
        }
    }

    componentDidMount(){
        this.props.auth.keycloak.init({onLoad: 'login-required', flow: 'standard', checkLoginIframeInterval: 1}).success(authenticated => {
            if (authenticated) {
                this.props.loginAction({
                    authenticated: authenticated,
                    user: this.props.auth.keycloak.hasRealmRole(ROLE.USER),
                    merchant: this.props.auth.keycloak.hasRealmRole(ROLE.MERCHANT)
                });
                setInterval(() => {
                    this.props.auth.keycloak.updateToken(10).error(() => this.props.auth.keycloak.logout());
                }, 10000);
            }
        });
    }

    handleLogout(){
        const out = confirm("Are you sure, you want to log out?");
        if (out) {
            this.props.auth.keycloak.logout();
        }
    }


    render() {
        return (
            <div className="col-md-6 col-md-offset-3">
                { this.props.auth.user ?
                    <div>
                        <h1>I am user!</h1>
                        <p>You're logged in with React!!</p>
                        <h3>All registered users:</h3>
                        <button onClick={this.handleLogout.bind(this)}>Logout</button>
                    </div>
                    :
                    <div>
                        <h1>I am merchant!</h1>
                        <p>You're logged in with React!!</p>
                        <h3>All registered users:</h3>
                        <button onClick={this.handleLogout.bind(this)}>Logout</button>
                    </div>
                }
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        auth: state.login
    }
}

const test = connect(mapStateToProps, {loginAction})(Protected);

export {test as Protected};