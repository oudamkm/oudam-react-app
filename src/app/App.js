import React from "react";
import { Router, Switch, Route} from "react-router-dom";
import { history } from '../_store/index';
import { Main } from './share';
import { Home } from './home';
import { Protected } from './protected';

export class App extends React.Component {
    constructor(props){
        super(props);
    }

    render(){
        return(
            <Router onUpdate={() => window.scrollTo(0, 0)} history={history}>
                <Switch>
                    <Route exact path="/" component={Main} />
                    <Route path="/home" component={Home} />
                    <Route path="/protected" component={Protected} />
                </Switch>
            </Router>
        )
    }
}
