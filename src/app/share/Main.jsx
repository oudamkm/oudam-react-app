import React from '../../../node_modules/react/index';

export class Main extends React.Component {
    constructor(props){
        super(props);
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-lg-12 paddingLR">
                        <nav className="navbar navbar-inverse">
                            <div className="container-fluid">
                                <div className="navbar-header">
                                    <span className="open-menu" style={{fontSize:'18px',cursor:'pointer'}}>&#9776;</span>
                                </div>
                                <ul className="nav navbar-nav navbar-right">
                                    <li><a href="#"><span className="glyphicon glyphicon-user"> </span> Sign Up</a></li>
                                    <li><a href="#"><span className="glyphicon glyphicon-log-in"> </span> Log Out</a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12 paddingLR">
                        <div id="main">
                            { this.props.children }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
