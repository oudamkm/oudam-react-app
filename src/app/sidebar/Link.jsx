import React, { Component } from "../../../node_modules/react";
import PropTypes from "../../../node_modules/prop-types";
import { NavLink } from "../../../node_modules/react-router-dom";
import cx from "../../../node_modules/classnames";
import {
    List,
    Collapse,
    ListItem,
    ListItemIcon,
    ListItemText
} from "../../../node_modules/@material-ui/core";
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import InboxIcon from '@material-ui/icons/MoveToInbox';

export default class Link extends Component {
    constructor() {
        super();
        this.state = {
            open: true
        };
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick () {
        this.setState({open: !this.state.open});
    }

    static activeRoute(routeName) {
        return location.pathname.indexOf(routeName) > -1;
    }

    render(){
        const { classes, color, routes} = this.props;
        return(
            <List className={classes.list}>
                {routes.map((prop, key) => {
                    if (prop.redirect) return null;
                    const listItemClasses = cx({
                        [" " + classes[color]]: Link.activeRoute(prop.path)
                    });
                    const whiteFontClasses = cx({
                        [" " + classes.whiteFont]: Link.activeRoute(prop.path)
                    });
                    return (
                        <NavLink
                            to={prop.path}
                            className={classes.item}
                            activeClassName="active"
                            key={key}
                        >
                            <ListItem button className={classes.itemLink + listItemClasses}>
                                <ListItemIcon className={classes.itemIcon + whiteFontClasses}>
                                    <prop.icon />
                                </ListItemIcon>
                                <ListItemText
                                    primary={prop.sidebarName}
                                    className={classes.itemText + whiteFontClasses}
                                    disableTypography={true}
                                />
                            </ListItem>
                        </NavLink>
                    );
                })}
                <ListItem button style={{color: 'white'}} onClick={this.handleClick}>
                    <ListItemIcon className={classes.itemIcon}>
                        <InboxIcon />
                    </ListItemIcon>
                    <ListItemText inset primary="Inbox" />
                    {this.state.open ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={this.state.open} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        <ListItem button className={classes.nested}>
                            <ListItemIcon>
                                <InboxIcon />
                            </ListItemIcon>
                            <ListItemText inset primary="Starred" />
                        </ListItem>
                    </List>
                </Collapse>
            </List>
        )
    }
}

Link.propTypes = {
    classes: PropTypes.object.isRequired
};