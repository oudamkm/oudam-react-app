import React from "../../../node_modules/react";
import PropTypes from "../../../node_modules/prop-types";
import { NavLink } from "../../../node_modules/react-router-dom";
import cx from "../../../node_modules/classnames";
import {
  withStyles,
  Drawer,
  Hidden,
  List,
    Collapse,
  ListItem,
  ListItemIcon,
  ListItemText
} from "../../../node_modules/@material-ui/core";
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import InboxIcon from '@material-ui/icons/MoveToInbox';

import sidebarStyle from "../../assets/jss/material-dashboard-react/sidebarStyle.jsx";

import Link from './Link';

const Sidebar = ({ ...props }) => {
  // verifies if routeName is the one active (in browser input)
    let state = {
      open: true
    };
  function activeRoute(routeName) {
    return props.location.pathname.indexOf(routeName) > -1;
  }
  function handleClick () {
     state.open = !state.open;
    }

  const { classes, color, logo, image, logoText, routes } = props;
  let links = (
    <List className={classes.list}>
      {routes.map((prop, key) => {
        if (prop.redirect) return null;
        const listItemClasses = cx({
          [" " + classes[color]]: activeRoute(prop.path)
        });
        const whiteFontClasses = cx({
          [" " + classes.whiteFont]: activeRoute(prop.path)
        });
        return (
          <NavLink
            to={prop.path}
            className={classes.item}
            activeClassName="active"
            key={key}
          >
            <ListItem button className={classes.itemLink + listItemClasses}>
              <ListItemIcon className={classes.itemIcon + whiteFontClasses}>
                <prop.icon />
              </ListItemIcon>
              <ListItemText
                primary={prop.sidebarName}
                className={classes.itemText + whiteFontClasses}
                disableTypography={true}
              />
            </ListItem>
          </NavLink>
        );
      })}
      <ListItem button className={classes.itemLink} onClick={() => handleClick()}>
        <ListItemIcon className={classes.itemIcon}>
          <InboxIcon />
        </ListItemIcon>
        <ListItemText inset primary="Inbox" />
          {state.open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={state.open} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          <ListItem button className={classes.nested}>
            <ListItemIcon>
              <InboxIcon />
            </ListItemIcon>
            <ListItemText inset primary="Starred" />
          </ListItem>
        </List>
      </Collapse>
    </List>
  );
  let brand = (
    <div className={classes.logo}>
      <a href="https://www.creative-tim.com" className={classes.logoLink}>
        <div className={classes.logoImage}>
          <img src={logo} alt="logo" className={classes.img} />
        </div>
        {logoText}
      </a>
    </div>
  );
  return (
    <div>
      <Hidden mdUp>
        <Drawer
          variant="temporary"
          anchor="right"
          open={props.open}
          classes={{
            paper: classes.drawerPaper
          }}
          onClose={props.handleDrawerToggle}
          ModalProps={{
            keepMounted: true // Better open performance on mobile.
          }}
        >
          {brand}
          <div className={classes.sidebarWrapper}>{links}</div>
          {image !== undefined ? (
            <div
              className={classes.background}
              style={{ backgroundImage: "url(" + image + ")" }}
            />
          ) : null}
        </Drawer>
      </Hidden>
      <Hidden smDown>
        <Drawer
            variant="permanent"
            anchor="left"
            open
            classes={{
              paper: classes.drawerPaper
            }}
        >
          {brand}
          <div className={classes.sidebarWrapper}>
              {
                props !== undefined ?
                    <Link classes={classes} color={color} routes={routes} />
                    :
                    null
              }
          </div>
          {image !== undefined ? (
            <div
              className={classes.background}
              style={{ backgroundImage: "url(" + image + ")" }}
            />
          ) : null}
        </Drawer>
      </Hidden>
    </div>
  );
};

Sidebar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(sidebarStyle)(Sidebar);
