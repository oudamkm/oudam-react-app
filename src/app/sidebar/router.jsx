import {
    Dashboard,
    Person,
    ContentPaste,
    LibraryBooks,
    BubbleChart,
    LocationOn,
    Notifications
} from "@material-ui/icons";

const router = [
    {
        path: "/home",
        sidebarName: "Dashboard",
        navbarName: "Material Dashboard",
        icon: Dashboard,
        component: null
    },
    {
        path: "/user",
        sidebarName: "User Profile",
        navbarName: "Profile",
        icon: Person,
        component: null
    },
    {
        path: "/table",
        sidebarName: "Table List",
        navbarName: "Table List",
        icon: ContentPaste,
        component: null
    },
    {
        path: "/typography",
        sidebarName: "Typography",
        navbarName: "Typography",
        icon: LibraryBooks,
        component: null
    },
    {
        path: "/icons",
        sidebarName: "Icons",
        navbarName: "Icons",
        icon: BubbleChart,
        component: null
    },
    {
        path: "/maps",
        sidebarName: "Maps",
        navbarName: "Map",
        icon: LocationOn,
        component: null
    },
    {
        path: "/notifications",
        sidebarName: "Notifications",
        navbarName: "Notifications",
        icon: Notifications,
        component: null
    },
    { redirect: true, path: "/", to: "/home", navbarName: "Redirect" }
];

export default router;
