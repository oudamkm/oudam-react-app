import React from "../../../node_modules/react";
import PropTypes from "../../../node_modules/prop-types";
import { Menu } from "../../../node_modules/@material-ui/icons";
import {
  withStyles,
  AppBar,
  Toolbar,
  IconButton,
  Hidden,
  Button
} from "../../../node_modules/@material-ui/core";
import cx from "../../../node_modules/classnames";

import headerStyle from "../../assets/jss/material-dashboard-react/headerStyle";

function Header({ ...props }) {
  function makeBrand() {
    if(props.routes !== undefined){
        props.routes.map(prop => {
            if (prop.path === props.location.pathname) {
              return prop.navbarName;
            }
            return null;
        });
    }
  }
  const { classes, color } = props;
  const appBarClasses = cx({
    [" " + classes[color]]: color
  });
  return (
    <AppBar className={classes.appBar + appBarClasses}>
      <Toolbar className={classes.container}>
        <div className={classes.flex}>
          {/* Here we create navbar brand, based on route name */}
          <Button href="#" className={classes.title}>
            {makeBrand()}
          </Button>
        </div>
        <Hidden mdUp>
          <IconButton
            className={classes.appResponsive}
            color="inherit"
            aria-label="open drawer"
            onClick={props.handleDrawerToggle}
          >
            <Menu />
          </IconButton>
        </Hidden>
      </Toolbar>
    </AppBar>
  );
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
  color: PropTypes.oneOf(["primary", "info", "success", "warning", "danger"])
};

export default withStyles(headerStyle)(Header);
