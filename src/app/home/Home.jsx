import React from '../../../node_modules/react';
import { Link } from '../../../node_modules/react-router-dom';
import Sidebar from '../sidebar/Sidebar';
import Router from '../sidebar/router';
import Header from '../header/Header';
import Footer from '../footer/Footer';
import CustomInput from '../../components/CustomInput/CustomInput';


export class Home extends React.Component {
    constructor(){
        super();
        this.state = {
            mobileOpen: false
        };
        this.handleDrawerToggle = this.handleDrawerToggle.bind(this);
    }

    handleDrawerToggle(){
        this.setState({ mobileOpen: !this.state.mobileOpen });
    };

    render() {
        const { classes, ...rest } = this.props;
        return (
            <div>
                <Header />
                <h1>Hi!</h1>
                <p>You're logged in with React!!</p>
                <h3>All registered users:</h3>
                <Sidebar
                    routes={Router}
                    logoText={"OUDAM"}
                    logo={'icons/reactlogo.png'}
                    image={'icons/sidebar-2.jpg'}
                    handleDrawerToggle={this.handleDrawerToggle}
                    open={this.state.mobileOpen}
                    color="blue"
                    {...rest}
                />
                <div>
                    <Header
                        routes={Router}
                        handleDrawerToggle={this.handleDrawerToggle}
                        {...rest}
                    />
                    <div> <CustomInput labelText="Full name"
                                       id="full_name"
                                       formControlProps={{fullWidth: true}}
                                       /> </div>
                    <p>
                        <Link to="/protected">Protected</Link>
                    </p>
                    <Footer />
                </div>
            </div>
        );
    }
}